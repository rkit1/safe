-- Up
CREATE TABLE Log (id INTEGER PRIMARY KEY, value JSON);
CREATE UNIQUE INDEX Log_serial_number ON Log ( json_extract(value, '$.serial_number') );
CREATE TABLE Settings (key STRING PRIMARY KEY, value JSON);
INSERT INTO Settings (key, value) VALUES ('serial_number', JSON('0'));
INSERT INTO Settings (key, value) VALUES ('server_serial_number', JSON('-1'));

-- Down
DROP TABLE Log;
DROP TABLE Settings;
