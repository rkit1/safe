import { crc16kermit, delay } from './util';
import SerialPort from 'serialport';
import { Transform, Writable } from 'stream';
import chalk from 'chalk';

export class BanknoteReader {
  async poll() {
    console.log('>poll');
    await this.port.write(Buffer.from([0xFC, 0x05, 0x11, 0x27, 0x56]));
    await this.port.drain();
  }

  async enable_all() {
    console.log('>enable');
    await this.send_packet(0xC0, [0x0, 0x0]);
  }

  async inhibit(b: boolean) {
    console.log('>inhibit_reader');
    await this.send_packet(0xC3, [b ? 1 : 0]);
  }


  async check_enabled() {
    console.log('>check_enabled')
    await this.send_packet(0x80);
  }

  async version_request() {
    console.log('>version_req')
    await this.send_packet(0x88);
  }

  async reset() {
    console.log('>reset')
    await this.send_packet(0x40);
  }

  async ack() {
    console.log('>ack');
    await this.send_packet(0x50);
  }

  async stack1() {
    console.log('>stack1')
    await this.send_packet(0x41);
  }

  async stack2() {
    console.log('>stack2')
    await this.send_packet(0x42);
  }

  async return_note() {
    console.log('>return_note')
    await this.send_packet(0x43);
  }


  /***
   * int1 - прерывания по любому случаю
   * int2 - прерывания только когда нужен ответ от контроллера
   */
  async set_comm_mode(mode: 'polling' | 'int1' | 'int2') {
    console.log('>comm_mode');
    await this.send_packet(0xC2, [mode === 'polling' ? 0 : mode === 'int1' ? 1 : 2]);
  }

  async send_packet(cmd: number, data: number[] = []) {
    await this.port.write(BanknoteReader.make_packet(cmd, data));
    await this.port.drain();
  }

  static make_packet(cmd: number, data: number[] = []) {
    const buf = Buffer.alloc(data.length + 5);
    buf[0] = 0xFC;
    buf[1] = buf.length;
    buf[2] = cmd;
    let n = 0;
    for (; n < data.length; n++) {
      buf[n + 3] = data[n];
    }
    n += 3;
    const crc = crc16kermit(buf.slice(0, n));
    buf[n++] = crc & 0xFF;
    buf[n++] = (crc & 0xFF00) / 256;
    return buf;
  }

  parser = new ID003Parser();

  constructor(private port: SerialPort) {
    port.pipe(this.parser as Writable);
  }

  async read_message(timeout?: number): Promise<ReaderMessage | null> {
    if (timeout === undefined) timeout = 0;
    while (true) {
      let d: Buffer;
      let data: Buffer;
      if ((d = this.parser.read()) !== null) {
        if (d[1] > 5) {
          data = d.slice(3, d[1] - 2);
        }
        const out = {
          type: d[2],
          data: data
        };
        BanknoteReader.printReaderMessage(out);
        return out;
      }
      if (timeout > 0) {
        const time = Math.min(timeout, 10);
        timeout -= time;
        await delay(time);
      } else {
        return null;
      }
    };
  }

  async specific_message(timeout: number, types: ReaderMessageType[]):
    Promise<ReaderMessage | null> {
    const msg = await this.read_message(timeout);
    if (msg !== null && types.find(t => t === msg.type)) {
      return msg;
    } else {
      throw "Неожиданный тип сообщения";
    }
  }

  static escrow_sum(message: ReaderMessage): number | null {
    if (message.type !== ReaderMessageType.ESCROW) throw "wrong argument";
    switch (message.data[0]) {
      case 0x61:
        return 200;
      case 0x62:
        return 2000;
      case 0x62:
        return 2000;
      case 0x63:
        return 10;
      case 0x64:
        return 50;
      case 0x65:
        return 100;
      case 0x66:
        return 500;
      case 0x67:
        return 1000;
      case 0x68:
        return 5000;
      default:
        return null;
    }
  }

  static printReaderMessage(message: ReaderMessage) {
    switch (message.type) {
      case ReaderMessageType.ESCROW:
        const sum = BanknoteReader.escrow_sum(message);
        if (sum !== null) {
          console.log("<ESCROW", chalk.red(sum));
        }
        else {
          console.log("<ESCROW UNKNOWN TYPE", chalk.red(message.data[0].toString(16)));
        }
        break;
      case ReaderMessageType.REJECTING:
        const reason = ReaderMessageREJECTINGType[message.data[0]];
        if (reason !== null) {
          console.log("<REJECTING", reason);
        }
        else {
          console.log("<REJECTING UNKNOWN TYPE", message.data[0].toString(16));
        }
        break;

      case ReaderMessageType.FAILURE:
        const reason1 = ReaderMessageFAILUREType[message.data[0]];
        if (reason1 !== null) {
          console.log("<FAILURE", reason1);
        }
        else {
          console.log("<FAILURE UNKNOWN TYPE", message.data[0].toString(16));
        }
        break;

        break;
      default:
        let dataString = "";
        let typeString = message.type.toString();
        if (ReaderMessageType[message.type] !== undefined) {
          typeString = ReaderMessageType[message.type];
        }
        if (message.data) {
          dataString = message.data.toString('hex');
        }
        console.log(`<${typeString} ${dataString}`);
    }
  }
}

export interface ReaderMessage {
  type: ReaderMessageType;
  data?: Buffer;
}


export enum ReaderMessageType {
  // status
  IDLE = 0x11,
  ACCEPTING = 0x12,
  ESCROW = 0x13,
  STACKING = 0x14,
  VEND_VALID = 0x15,
  STACKED = 0x16,
  REJECTING = 0x17,
  RETURNING = 0x18,
  HOLDING = 0x19,
  READER_DISABLE = 0x1A,
  INITIALIZE = 0x1B,

  // power up status
  POWER_UP = 0x40,
  POWER_UP_BILL_IN_ACCEPTOR = 0x41,
  POWER_UP_BILL_IN_STACKER = 0x42,

  // error status
  STACKER_FULL = 0x43,
  STACKER_OPEN = 0x44,
  JAM_IN_ACCEPTOR = 0x45,
  JAM_IN_STACKER = 0x46,
  PAUSE = 0x47,
  CHEATED = 0x48,
  FAILURE = 0x49,
  COMMUNICATION_ERROR = 0x4A,

  // poll request
  ENQ = 0x05,

  // response to operation command
  ACK = 0x50,
  INVALID_COMMAND = 0x48,

  // response to setting command
  ENABLE_DENOMINATION_RESPONSE = 0xC0,
  ENABLE_SECURITY_RESPONSE = 0xC1,
  COMMUNICATION_MODE_RESPONSE = 0xC2,
  INHIBIT_RESPONSE = 0xC3,
  DIRECTION_RESPONSE = 0xC4,
  OPTIONAL_FUNCTION_RESPONSE = 0xC5,

  // setting status
  ENABLE_DENOMINATION_STATUS = 0x80,
  ENABLE_SECURITY_STATUS = 0x81,
  COMMUNICATION_MODE_STATUS = 0x82,
  INHIBIT_STATUS = 0x83,
  DIRECTION_STATUS = 0x84,
  OPTIONAL_FUNCTION_STATUS = 0x85,
  VERSION_INFORMATION = 0x88,
  BOOT_VERSION_INFORMATION = 0x89,
  DENOMINATION_DATA = 0x8A,
}


/* A status of returning bills due to the discrimination of
 * unacceptable bills and/or [INHIBIT] command from CONTROLLER. (See
 * 7-3) */
export enum ReaderMessageREJECTINGType {
  INSERTION_ERROR = 0x71,
  MUG_ERROR = 0x72,
  RESIDUAL_BILL = 0x73,
  CALIBRAION_ERROR = 0x74,
  CONVEYING_ERROR = 0x75,
  DISCRIMINATION_ERROR = 0x76,
  PHOTO_PATTERN_ERROR_1 = 0x77,
  PHOTO_LEVEL_ERROR = 0x78,
  RETURN_BY_INHIBIT = 0x79,
  OPERATION_ERROR = 0x7B,
  RETURN_DUE_TO_RESIDUAL_BILL = 0x7C,
  LENGHT_ERROR = 0x7D,
  PHOTO_PATTERN_ERROR_2 = 0x7E,
  TRUE_BILL_FEATURE_ERROR = 0x7F,
}


export enum ReaderMessageFAILUREType {
  sTACK_MOTOR_FAILURE = 0xA2,
  TRANSPORT_MOTOR_SPEED_FAILURE = 0xA5,
  TRANSPORT_MOTOR_FAILURE = 0xA6,
  SOLENOID_FAILURE = 0xA8,
  PB_UNIT_FAILURE = 0xA9,
  CASH_BOX_NOT_READY = 0xAB,
  VALIDATOR_HEAD_REMOVE = 0xAF,
  BOOT_ROM_FAILURE = 0xB0,
  EXTERNAL_ROM_FAILURE = 0xB1,
  RAM_FAILURE = 0xB2,
  EXTERNAL_ROM_WRITING_FAILURE = 0xB3,
}

// sync, len, cmd, data[n], crc[2]
export class ID003Parser extends Transform {
  status: 'error' | 'parsing' = 'error'
  buffer = Buffer.alloc(0);

  constructor(options = {}) {
    super(options)
  }

  _transform(chunk: Buffer, encoding: any, cb: () => any) {
    let data = Buffer.concat([this.buffer, chunk])
    //console.log('got chunk', chunk);
    while (data.length) {
      //console.log('len');
      //console.log(data);
      if (this.status === 'error') {
        const position = data.indexOf(0xFC);
        if (position === -1) {
          console.log('rejecting1', data);
          this.buffer = Buffer.alloc(0);
          return cb();
        } else {
          if (position > 0) {
            console.log('rejecting2', data.slice(0, position));
          }
          data = data.slice(position);
          this.status = 'parsing';
        }
      }
      if (data.length >= 2 && data.length >= data[1]) {
        const crc = crc16kermit(data.slice(0, data[1] - 2));
        if (data[data[1] - 2] === (crc & 0xFF)
          && data[data[1] - 1] === ((crc & 0xFF00) / 256)) {
          //console.log('pushing', data.slice(0, data[1]));
          this.push(data.slice(0, data[1]));
          data = data.slice(data[1]);
          this.status = 'error';
        }
      } else {
        this.buffer = data;
        return cb();
      }
    }
    this.buffer = Buffer.alloc(0);
    return cb();
  }

  _flush(cb: () => any) {
    cb();
  }
}

