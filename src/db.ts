import sqlite from 'sqlite';
import { EventEmitter } from 'events';
import { delay } from './util';
import moment from 'moment';
import { OutgoingMessage } from './log';
import chalk from 'chalk';

export default class DB extends EventEmitter {
  private static instance: DB;
  private static init_done = false;
  private constructor() {
    super();
  }

  private sqlite_instance: any;

  static async get_instance() {
    if (!DB.instance) {
      DB.instance = new DB();
      DB.instance.sqlite_instance = await sqlite.open('./db.sqlite');
      try {
        await DB.instance.sqlite_instance.exec("ROLLBACK");
        console.log(chalk.red('wtf transaction'));
      } catch (err) {

      }
      //force: 'last', 
      await DB.instance.sqlite_instance.migrate({ migrationsPath: './sql' });
      setInterval(DB.cleanup, 1000*60*60*24);
      await DB.cleanup();
      DB.init_done = true;
    } else {
      while (!DB.init_done) await delay(10);
    }
    return DB.instance;
  }


  /**
   * Удаляем старые логи
   **/
  private static async cleanup() {
    const db = DB.instance.sqlite_instance;
    const t = moment().subtract(3, 'months').format('x');
    const x = await db.run("DELETE FROM Log WHERE json_extract(value, '$.time') < ?", [t]);
    console.log(`DB cleanup: deleted ${x.stmt.changes} rows`);
  }

  /**
   * Возвращает и увеличивает счетчик записей в логе. 
   */
  private async aquire_serial(): Promise<number> {
    const db = this.sqlite_instance;
    const res = await db.get("SELECT value FROM Settings WHERE key = 'serial_number'");
    const num = Number(res.value);
    await db.run(`
UPDATE Settings SET value = json(?) WHERE key = 'serial_number'`,
                 [JSON.stringify(num + 1)]);
    return num;
  }

  /**
   * Заполняет системные поля и добавляет новую запись в лог.
   */
  async log(message: Partial<OutgoingMessage>) {
    const db = this.sqlite_instance;
    message.time = moment().format('X');
    message.machine_id = "VezOAefqMz7WkIxG6Xur3LA0nvZbN2uo";
    try {
      await db.exec("BEGIN TRANSACTION");
      const serial = await this.aquire_serial();
      message.serial_number = serial;
      await db.run("INSERT INTO Log (value) VALUES (json(?))",
        [JSON.stringify(message)]);
      await db.exec("COMMIT");
    } catch (err) {
      console.error("DB.log: ", err);
      await db.exec("ROLLBACK");
      throw (err);
    }
    this.emit('new_log_entry');
  }

  /**
   * Возвращает последнюю запись в логе, не отправленную на сервер
   */
  async next_log_entry(): Promise<OutgoingMessage | null> {
    const db = this.sqlite_instance;
    const res = await db.get("SELECT value FROM Settings WHERE key = 'server_serial_number'");
    const res1 = await db.get(
      `SELECT value FROM Log 
WHERE json_extract(value, '$.serial_number') > ?
ORDER BY json_extract(value, '$.serial_number') ASC
LIMIT 1;`, [res.value]
    );
    if (res1) return JSON.parse(res1.value);
    return null;
  }

  /**
   * Увеличивает счетчик, отслеживающий последнее собщение, принятое сервером
   */
  async advance_server_serial() {
    const db = this.sqlite_instance;
    await db.exec(`
UPDATE Settings SET value = json(json_extract(value, '$') + 1)
WHERE key = 'server_serial_number';
`);
  }

  /**
   * Вываливает весь лог в консоль
   */
  async dump() {
    const db = this.sqlite_instance;
    const res = await db.all("SELECT id, value FROM Log ORDER BY id ASC");
    res.forEach((x: any) => console.log(x.value));
  }
}
