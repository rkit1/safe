import { BanknoteReader, ReaderMessage, ReaderMessageType } from "./banknote_reader";
import DBClass from "./db";
import Server from "./remote";
import { EventEmitter } from "events";
import { RepeatTimeout } from "./util";

export default class Log extends EventEmitter {
  private static instance: Log;
  static get_instance(): Log {
    if (!Log.instance) Log.instance = new Log();
    return Log.instance;
  }

  private constructor() {
    super();
  }


  srt = new RepeatTimeout();
  async log_status(status: string) {
    const entry = {
      type: "system_status",
      value: status,
    };
    this.srt.trigger(status, 1000 * 60 * 10, async () => {
      const db = await DBClass.get_instance();
      await db.log(entry);
    });
  }

  nrt = new RepeatTimeout();
  async log_network(status: boolean) {
    const entry = {
      type: "network_status",
      value: status ? "online" : "communication_error",
    };
    this.nrt.trigger(status, 1000 * 60 * 10, async () => {
      const db = await DBClass.get_instance();
      await db.log(entry);
    });
  }
}

// Общие поля для всех сообщений
export interface OutgoingMessage {
  type: string;
  time: string; // utc timestamp
  serial_number: number; // Строго последовательная нумерация. 
  machine_id: string;
}

// Принял одну банкноту
export interface BanknotesAccepted extends OutgoingMessage {
  type: "banknotes_accepted";
  value: number; // Номинал банкноты.
}

// Напечатал чек
export interface CheckIsPrinted extends OutgoingMessage {
  type: "check_is_printed";
  banknote_operations: number[]; // Все serial_number внесенных банкнот
  value: number; // Сумма на чеке.
}

export interface NetworkStatus extends OutgoingMessage {
  type: "network_status";
  value: "online" | "communcation_error";
}

export interface SystemStatus extends OutgoingMessage {
  type: "system_status";
  value: "idle" // ожидание вноса
  | "starutp" // Отправляется только один раз при пуске. 
  | "initializing" // ИНициализация куплюроприемника и других систем. 
  | "stacker_open" // изъята коробка купюроприемника
  | "accepting" // В процессе приема денег, продолжается до печати чека
  | "failure" // Серьезный сбой; требуется вмешательство.
  ;
}

