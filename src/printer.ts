import chalk from 'chalk';
import { spawn } from 'child_process';

export default class Printer {
  print(text: string) {
    if (process.platform === "win32") {
      console.log(chalk.green(text)); // Заглушка для разработки на винде
    } else if (process.platform === "linux") {
      console.log(chalk.green(text));
      const lp = spawn('lp', ['-o', 'media=om_x-80-mmy-90-mm_77.61x89.7mm']);
      lp.stdin.write(text);
      lp.stdin.end();
    }
  }
}
