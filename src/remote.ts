import request from 'request-promise-native';
import { delay } from './util';
import DBClass from './db';
import { once } from 'events';
import Log from "./log";

export default class Server {
  uri = "http://dashboard.integit.ru/safe/scripts/api";

  async send(message: Object) {
    const res = await request.post({
      method: 'POST',
      uri: this.uri,
      json: true,
      body: message,
      timeout: 15000,
    });
    if (!res || !res.success) {
      throw { message: message, server_error_response: res };
    } else {
      console.log({ message: message, server_response: res });
    }
  }

  async loop() {
    const log = Log.get_instance();
    const DB = await DBClass.get_instance();
    while (true) {
      try {
        const e = await DB.next_log_entry();
        if (e != null) {
          await this.send(e);
          await DB.advance_server_serial();
          log.emit('network', true);
        } else {
          await once(DB, 'new_log_entry');
        }
      } catch (err) {
        log.emit('network', err);
        console.error(err);
        await delay(60000);
      }
    }
  }

  constructor() {
    this.loop();
  }
}
