import SerialPort from 'serialport';
import { BanknoteReader, ReaderMessageType } from './banknote_reader';
import { delay, InputTimeout } from './util';
import chalk from 'chalk';
import moment from 'moment';
import Printer from './printer';
import Log, { OutgoingMessage, BanknotesAccepted } from "./log";
import DBClass from './db';
import Server from './remote';

const port_string =
  process.platform === "win32" ? 'COM22' :
    process.platform === "linux" ? '/dev/ttyUSB0' :
      null;

const port = new SerialPort(port_string, {
  baudRate: 9600,
  dataBits: 8,
  stopBits: 1,
  parity: 'even'
});

port.on('error', function(err: { message: string }) {
  console.error('Error: ', err.message);
})


const reader = new BanknoteReader(port);

async function loop() {
  Log.get_instance().log_status("startup");
  const DB = await DBClass.get_instance();
  await reader.poll();
  while (true) {
    try {
      const msg = await reader.read_message(30000);
      if (msg !== null) {
        switch (msg.type) {
          case ReaderMessageType.IDLE:
            if (timeout.running) {
              await Log.get_instance().log_status("accepting");
            } else {
              await Log.get_instance().log_status("idle");
            }
            break;

          case ReaderMessageType.ENQ:
            await reader.ack();
            await reader.poll();
            break;
          case ReaderMessageType.POWER_UP:
          case ReaderMessageType.POWER_UP_BILL_IN_ACCEPTOR:
          case ReaderMessageType.POWER_UP_BILL_IN_STACKER:
            //await reader.version_request(); console.log('>version');
            await reader.reset();
            break;
          case ReaderMessageType.INITIALIZE:
          case ReaderMessageType.READER_DISABLE:
            await initialize();
            break;
          case ReaderMessageType.ESCROW:
            await Log.get_instance().log_status("accepting");
            let message = {
              value: BanknoteReader.escrow_sum(msg),
              type: "banknotes_accepted",
            }
            await accept();
            await DB.log(message);
            //await reject();
            timeout.trigger();
            money_transaction.push(message as BanknotesAccepted);
            break;

          case ReaderMessageType.STACKER_OPEN:
            Log.get_instance().log_status("stacker_open");
            break;

          case ReaderMessageType.STACKER_FULL:
          case ReaderMessageType.JAM_IN_ACCEPTOR:
          case ReaderMessageType.JAM_IN_STACKER:
          case ReaderMessageType.CHEATED:
          case ReaderMessageType.FAILURE:
          case ReaderMessageType.COMMUNICATION_ERROR:
            await Log.get_instance().log_status("failure");
            break;


          case ReaderMessageType.PAUSE:
          case ReaderMessageType.HOLDING:
          case ReaderMessageType.STACKING:
          case ReaderMessageType.STACKED:
          case ReaderMessageType.REJECTING:
          case ReaderMessageType.RETURNING:
            //trace wtf
            await reject();
            break;

        }
      } else {
        reader.poll();
      }
    } catch (err) {
      console.error(err);
      await delay(15000);
      await reader.reset();
    }
  }
}


async function initialize() {
  Log.get_instance().log_status("initializing");
  await reader.enable_all();
  await reader.specific_message(2000,
    [ReaderMessageType.ENABLE_DENOMINATION_RESPONSE]);
  await reader.set_comm_mode('int2');
  await reader.specific_message(2000,
    [ReaderMessageType.COMMUNICATION_MODE_RESPONSE]);
  await reader.inhibit(false);
  await reader.specific_message(2000,
    [ReaderMessageType.INHIBIT_RESPONSE]);
}

async function reject() {
  await reader.return_note();
  await reader.specific_message(2000,
    [ReaderMessageType.ACK]);

  while (true) {
    await reader.poll();
    const msg = await reader.read_message(50);
    switch (msg.type) {
      case ReaderMessageType.ENQ:
        break;
      case ReaderMessageType.RETURNING:
        await delay(500);
        break;
      case ReaderMessageType.IDLE:
        return;
      default:
        throw "Неожиданный тип сообщения";
    }
  }
}


async function accept() {
  await reader.stack1();
  await reader.specific_message(2000,
    [ReaderMessageType.ACK]);

  while (true) {
    const msg = await reader.read_message(5000);
    switch (msg.type) {
      case ReaderMessageType.PAUSE:
      case ReaderMessageType.ENQ:
        await reader.poll();
        break;
      case ReaderMessageType.STACKING:
        break;
      case ReaderMessageType.VEND_VALID:
        await reader.ack();
        return;
      case ReaderMessageType.STACKER_FULL:
      default:
        throw "Неожиданный тип сообщения";
    }
  }
}



process.on('unhandledRejection', up => { throw up });
chalk.level = 2;
const timeout = new InputTimeout(15000);
const printer = new Printer();
let money_transaction: BanknotesAccepted[] = [];
moment.locale('ru');
timeout.on('complete', async () => {
  const DB = await DBClass.get_instance();
  const sum = money_transaction.map(x => x.value).reduce((x, y) => x + y);
  let s = "";
  const date = moment().format('L LT');
  s += `Дата: ${date}\r\n`;
  s += `Сумма: ${sum}\r\n`;
  printer.print(s);
  const data = {
    type: "check_is_printed",
    banknote_operations: money_transaction.map(x => x.serial_number),
    value: sum,
  }
  money_transaction = [];
  await DB.log(data);
});
loop();
new Server();
